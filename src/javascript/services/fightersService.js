import {callApi} from "../helpers/apiHelper";

class FighterService {
    async getFighters() {
        try {
            const endpoint = 'repos/sahanr/street-fighter/contents/fighters.json';
            const apiResult = await callApi(endpoint, 'GET');
            return JSON.parse(atob(apiResult.content));
        } catch (error) {
            throw error;
        }
    }

    async getFighterDetails(_id){
        // implement this method
        // endpoint - `details/fighter/${_id}.json`;
        try{
            const endpoint = `repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/details/fighter/${_id}.json`;
            const apiResult = await callApi(endpoint, "GET");

            return JSON.parse(atob(apiResult.content));
        } catch (error) {
            throw error
        }
    }
}

export const fighterService = new FighterService();

