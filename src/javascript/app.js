import FightersView from "./fightersView"
import {fighterService} from "./services/fightersService";
import StartFightView from "./startFightView"
import HeaderTextView from "./headerText"

class App {
    constructor() {
        this.startApp();
    }

    static rootElement = document.getElementById('root');
    static loadingElement = document.getElementById('loading-overlay');

    async startApp() {
        try {
            App.loadingElement.style.visibility = 'visible';

            const fighters = await fighterService.getFighters();
            const fightersView = new FightersView(fighters);
            const startButton = new StartFightView(fightersView);
            const headerText = new HeaderTextView();
            const headerTextElement = headerText.element
            const fightersElement = fightersView.element;
            const startButtonElement = startButton.element;

            App.rootElement.appendChild(headerTextElement);
            App.rootElement.appendChild(fightersElement);
            App.rootElement.appendChild(startButtonElement);
        } catch (error) {
            App.rootElement.innerText = 'Failed to load data';
        } finally {
            App.loadingElement.style.visibility = 'hidden';
        }
    }
}

export default App