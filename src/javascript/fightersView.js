import View from "./view"
import FighterView from "./fighterView"
import {fighterService} from "./services/fightersService";
import FighterEditView from "./fighterEditView"



class FightersView extends View {
    constructor(fighters) {
        super();

        this.handleFighterClick = this.handleFighterClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.createFighters(fighters);
    }

    fightersDetailsMap = new Map();

    createFighters(fighters) {
        const fighterElements = fighters.map(fighter => {
            const fighterView = new FighterView(fighter, this.handleFighterClick);
            return fighterView.element;
        });

        this.element = this.createElement({ tagName: 'form', className: 'fighters', attributes: {id: "fighters"}});
        this.element.append(...fighterElements);
    }
    handleChange(event, fighter){
        this.fightersDetailsMap.set(fighter._id, fighter);
    }
    getFighter(id){
        if(!this.fightersDetailsMap.has(id)){
            // const fighterDetail = await fighterService.getFighterDetails(id);
            const fighterDetail = this.loadFighter(id);
            this.fightersDetailsMap.set(id, fighterDetail);
        }
        return this.fightersDetailsMap.get(id);
    }
    async loadFighter(fighterId){
        return await fighterService.getFighterDetails(fighterId);
    }
    async handleFighterClick(event, fighter) {
        const currentFighter = await this.getFighter(fighter._id);
        const modalView = new FighterEditView(currentFighter, this.handleChange);
        this.element.append(modalView.createModal());

        // get from map or load info and add to fightersMap
        // show modal with fighter info
        // allow to edit health and power in this modal
    }
}

export default FightersView