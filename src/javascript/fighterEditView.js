import View from "./view"

class FighterEditView extends View {
    constructor(fighter, updateFighterCallback) {
        super();
        this.fighter  = fighter;
        this.updateFighterCallback = updateFighterCallback;
    }
    handleChange(event, key){
        this.fighter[key] = event.target.value
    }
    saveChange(event){
        this.updateFighterCallback(event, this.fighter);
        // this.modalElement.parentNode.removeChild(this.modalElement);
        this.modalElement.remove()
    }
    fighter;
    updateFighterCallback;
    modalElement;
    createModal() {
        const modalWrapperElement = this.createElement({ tagName: 'div', className: 'modal-wrapper' });

        const modalElement = this.createElement({ tagName: 'div', className: 'modal' });


        Object.keys(this.fighter)
            .filter(key => !['source', '_id','name'].includes(key))
            .map(key => {
            const modalLabel = this.createElement({tagName: 'div', className: "label"})
            const modalField = this.createElement({ tagName: 'input', className: 'field', attributes:{id: key, value: this.fighter[key]} });
                modalField.addEventListener('change', event => this.handleChange(event, key), false);
            modalLabel.innerText = key
            modalElement.append(modalLabel, modalField)
        })
        const saveButton = this.createElement({tagName:'button', className:'save-button'});
        saveButton.addEventListener('click',event => this.saveChange(event), false);
        saveButton.innerText = 'save';
        modalElement.append(saveButton);
        modalWrapperElement.append(modalElement)
        this.modalElement = modalWrapperElement;
        return modalWrapperElement;
    }
}
export default FighterEditView