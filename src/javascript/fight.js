export default function fight(fighter1, fighter2) {
    let health1 = +fighter1.health;
    let health2 = +fighter2.health;
    let firstFighterTurn = true
    const makeHit = (damageDealer, damageTaker) => {
        const hitPower = damageDealer.getHitPower();
        const blockPower = damageTaker.getBlockPower()
        const damage = (hitPower - blockPower) > 0 ? (hitPower - blockPower) : 0;
        document.getElementById("fight-text").innerText += `${damageDealer.name} hits opponent for ${damage}`;
        const br = document.createElement("br");
        document.getElementById("fight-text").append(br);
        return damage
    };

    const fighting = () => {
        firstFighterTurn ? health2 -= makeHit(fighter1, fighter2) : health1 -= makeHit(fighter2, fighter1);
        firstFighterTurn ? document.getElementById(fighter1._id + "health").innerText = health1.toString() : document.getElementById(fighter2._id + "health").innerText = health2.toString()
        firstFighterTurn = !firstFighterTurn;
    }

    let intr = setInterval(function() {
        fighting()
        // clear the interval if `i` reached 100
        if (!someoneIsAlive(health1, health2)) {
            clearInterval(intr)
            document.getElementById("header-text").innerText = health1 <= 0 ? `${fighter2.name} win`: `${fighter1.name} win`
        }
    }, 300);

}

function someoneIsAlive(health1, health2) {
    return health1 > 0 && health2 > 0;
}
