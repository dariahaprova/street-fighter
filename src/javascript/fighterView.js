import View from "./view"

class FighterView extends View {
    constructor(fighter, handleClick) {
        super();

        this.createFighter(fighter, handleClick);
    }

    createFighter(fighter, handleClick) {
        const { name, source, _id } = fighter;
        const nameElement = this.createName(name);
        const imageElement = this.createImage(source);
        const inputElement = this.createInput(_id);

        this.element = this.createElement({ tagName: 'div', className: 'fighter' });
        this.element.append(imageElement, nameElement, inputElement);
        imageElement.addEventListener('click', event => handleClick(event, fighter), false);
    }

    createName(name) {
        const nameElement = this.createElement({ tagName: 'span', className: 'name' });
        nameElement.innerText = name;

        return nameElement;
    }

    createImage(source) {
        const attributes = { src: source, alt: "fighter" };
        const imgElement = this.createElement({
            tagName: 'img',
            className: 'fighter-image',
            attributes
        });

        return imgElement;
    }
    createInput(id) {
        const attributes = { id: id, type: "checkbox", name: "fighter", value: id };
        const inputElement = this.createElement({
            tagName: 'input',
            className: 'fighter-checkbox',
            attributes
        });

        return inputElement;
    }
}
export default FighterView