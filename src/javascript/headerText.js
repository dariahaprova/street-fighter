import View from "./view";

class HeaderTextView extends View{
    constructor() {
        super();
        this.createText();
    }
    createText(){
        this.element = this.createElement({ tagName: 'h1', className: 'header-text', attributes:{id: 'header-text'}});
        this.element.innerText = "Please, choose 2 fighters to start"

    }

}

export default HeaderTextView