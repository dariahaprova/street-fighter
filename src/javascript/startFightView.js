import View from "./view"
import fight from "./fight"
import Fighter from "./Fighter"
import SelectedFighterView from "./selectedFighter"

class StartFightView extends View{
    constructor(fightersView) {
        super();
        this.createButton();
        this.fightersView = fightersView;
    }

    createButton(){
        this.element = this.createElement({ tagName: 'div', className: 'fight-field'});
        const buttonElement= this.createElement({ tagName: 'button', className: 'start-button', attributes: {value: "start", id: "start-btn"} });
        buttonElement.innerText = "start";
        buttonElement.addEventListener('click', () => this.handleClick());
        this.element.append(buttonElement)

    }
    fightersView;
    async handleClick(){
        let values = [].filter.call(document.getElementsByName('fighter'), function(c) {
            return c.checked;
        }).map(function(c) {
            return c.value;
        });
        if(values.length === 2){
           const firstFighterData =  await this.fightersView.getFighter(values[0]);
           const secondFighterData = await this.fightersView.getFighter(values[1]);
           const firstFighter = new Fighter(firstFighterData);
           const secondFighter = new Fighter(secondFighterData);
           const selectedFighter1 = new SelectedFighterView(firstFighterData).element;
           const selectedFighter2 = new SelectedFighterView(secondFighterData).element;
           const fightButton = this.createElement({ tagName: 'button', className: 'fight-button', attributes: {value: "start", id: "fight-btn"} });
           fightButton.innerText = "start fight"
           const fightText = this.createElement({ tagName: 'div', className: 'fight-text', attributes: { id: "fight-text"} });
           const fightTextWrapper = this.createElement({ tagName: 'div', className: 'wrapper-fight-text', attributes: { id: "fight-text-wrapper"} });
           fightTextWrapper.append(fightText)
            document.getElementById('fighters').remove()
            document.getElementById('start-btn').remove()
            document.getElementById('header-text').innerText = ""

            this.element.append(selectedFighter1, selectedFighter2, fightButton, fightTextWrapper)
            fightButton.addEventListener('click', () => fight(firstFighter, secondFighter));


        }else{
            alert("you should choose 2 fighters to start")
        }
    }

}
export default StartFightView