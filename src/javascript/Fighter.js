class Fighter{
    constructor(fighter){
        this.name = fighter.name
        this.health = fighter.health
        this.attack = fighter.attack
        this.defense = fighter.defense
        this._id = fighter._id
    }
    name;
    health;
    attack;
    defense;
    _id;

    getHitPower(){
        const criticalHitChance = this.randomInteger(1,2);
        return +(this.attack * criticalHitChance);
    }
    randomInteger(min, max) {
        let rand = min - 0.5 + Math.random() * (max - min + 1);
        rand = Math.round(rand);
        return +rand;
    }
    getBlockPower(){
        const dodgeChance = this.randomInteger(1,2);
        return +(this.defense * dodgeChance);
    }
}
export default Fighter