import View from "./view"

class SelectedFighterView extends View {
    constructor(fighter) {
        super();

        this.createFighter(fighter);
    }

    createFighter(fighter, handleClick) {
        const { name, source, health, _id } = fighter;
        const nameElement = this.createName(name);
        const imageElement = this.createImage(source);
        const healthElement = this.createHealthIndicator(health, _id);

        this.element = this.createElement({ tagName: 'div', className: 'fighter' });
        this.element.append(imageElement, nameElement, healthElement);
        // imageElement.addEventListener('click', event => handleClick(event, fighter), false);
    }

    createName(name) {
        const nameElement = this.createElement({ tagName: 'span', className: 'name' });
        nameElement.innerText = name;

        return nameElement;
    }

    createImage(source) {
        const attributes = { src: source };
        const imgElement = this.createElement({
            tagName: 'img',
            className: 'fighter-image',
            attributes
        });

        return imgElement;
    }
    createHealthIndicator(health, _id) {
        const healthElement = this.createElement({ tagName: 'span', className: 'health', attributes:{id: _id + "health"}});
        healthElement.innerText = health;
        return healthElement;

    }
}
export default SelectedFighterView